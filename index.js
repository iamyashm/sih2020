const express = require('express');
const routes = require('./routes');
const dotenv = require('dotenv');
const bodyParser = require('body-parser');
const cookieParser = require('cookie-parser');
const ejs = require('ejs');

var app =  express();
dotenv.config();

app.set('view engine', 'ejs');
app.use(express.static(__dirname  + '/public'));
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(cookieParser());
app.use('/', routes);

app.listen(process.env.PORT || 6769, () =>{
    console.log("Listening....");
})
