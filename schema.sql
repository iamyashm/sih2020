drop table if exists grievance;
drop table if exists student;

create table student (
    reg_no int primary key,
    name varchar(20) not null,
    email varchar(30) not null unique,
    password varchar(20) not null,
    college varchar(40) not null,
    stream varchar(20) not null,
    sem smallint not null
);


create table grievance (
    id int auto_increment primary key,
    reg_no int not null,
    app_date date not null,
    level varchar(20) not null,
    category varchar(30) not null,
    details varchar(1000) not null,
    status smallint not null,
    response varchar(1000),
    foreign key(reg_no) references student(reg_no) on delete cascade
);


drop table if exists u_member;
create table u_member (
    id int auto_increment primary key,
    name varchar(20) not null,
    email varchar(30) not null unique,
    password varchar(20) not null,
    head tinyint default 0
);

drop table if exists i_member;
create table i_member (
    id int auto_increment primary key,
    name varchar(20) not null,
    email varchar(30) not null unique,
    password varchar(20) not null,
    head tinyint default 0
);

drop table if exists d_member;
create table d_member (
    id int auto_increment primary key,
    name varchar(20) not null,
    email varchar(30) not null unique,
    password varchar(20) not null,
    head tinyint default 0
);
