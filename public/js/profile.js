
function loginUser() {
    var fetchedJSON;
    fetch('/api/getProfile', {
        method: 'GET',
        headers: {
            "Content-Type" : "application/json",
        },
    }) .then(res => {
        return res.json();
    }) .then( function (json) {
        fetchedJSON = json;
        console.log(fetchedJSON)
        if (fetchedJSON["type"] === "student") {
            studentLogin(fetchedJSON);
            document.getElementById("addGr").style.display = "inline-block";
        }
        else if (fetchedJSON["type"] === "Committee Head") {
            document.getElementById("addMem").style.display = "inline-block";
            headLogin(fetchedJSON);
        }
        else if (fetchedJSON["type"] === "Committee Member")
            memberLogin(fetchedJSON);
    });
}

function logout() {
    fetch('/logout', {
        method: 'GET',
        headers: {
            "Content-Type": "application/json",
        },
    }).then(res => {
        return res.json();
    }).then(function (json) {
        window.location.replace('/');
    });
}


function studentLogin(fetchedJSON) {
    document.querySelector('#studentLogin').classList.remove("displayInactive");
    document.querySelector('#memberLogin').classList.add("displayInactive");
    document.querySelector('#headLogin').classList.add("displayInactive");

    const studentProfileJSON = fetchedJSON;
    document.querySelector('#studentName').innerHTML = "Name: " + studentProfileJSON["name"];
    document.querySelector('#studentCollege').innerHTML = "College: " + studentProfileJSON["college"];
    document.querySelector('#studentReg_no').innerHTML = "Registration Number: " + studentProfileJSON["reg_no"];
    document.querySelector('#studentStream').innerHTML = "Stream: " + studentProfileJSON["stream"];
    document.querySelector('#studentSem').innerHTML = "Semester: " + studentProfileJSON["sem"];
}

function headLogin(fetchedJSON) {
    document.querySelector('#studentLogin').classList.add("displayInactive");
    document.querySelector('#memberLogin').classList.add("displayInactive");
    document.querySelector('#headLogin').classList.remove("displayInactive");

    const headProfileJSON = fetchedJSON;

    document.querySelector('#headName').innerHTML = "Name: " + headProfileJSON["name"];
    document.querySelector('#headEmail').innerHTML = "E-Mail: " + headProfileJSON["email"];
}

function memberLogin(fetchedJSON) {
    document.querySelector('#studentLogin').classList.add("displayInactive");
    document.querySelector('#memberLogin').classList.remove("displayInactive");
    document.querySelector('#headLogin').classList.add("displayInactive");

    const memberProfileJSON = fetchedJSON;

    document.querySelector('#memberName').innerHTML = "Name: " + memberProfileJSON["name"];
    document.querySelector('#memberEmail').innerHTML = "E-Mail: " + memberProfileJSON["email"];
}
