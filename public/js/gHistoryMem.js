var curr_rec;
var curr_row;
var fetchedJSON;
function intotable() {

    fetch('/getGrievance', {
        method: 'GET',
        headers: {
        "Content-Type": "application/json",
        },
    }).then(res => {
      return res.json();
    }).then(function (json) {
        fetchedJSON = json;
    console.log(fetchedJSON)
    var x = document.getElementById("tbl");
    for (var i = 0; i < fetchedJSON.length; i = i + 1) {
        var row = x.insertRow(1 + i);
        var cell1 = row.insertCell(0);
        var cell2 = row.insertCell(1);
        var cell3 = row.insertCell(2);
        var cell4 = row.insertCell(3);
        var cell5 = row.insertCell(4);
        var cell6 = row.insertCell(5);
        var cell7 = row.insertCell(6);
        cell1.innerHTML = fetchedJSON[i].id;
        cell2.innerHTML = fetchedJSON[i].reg_no;
        cell3.innerHTML = fetchedJSON[i].details;
        cell4.innerHTML = fetchedJSON[i].app_date.substring(0,10);
        cell5.innerHTML = fetchedJSON[i].status?'Closed':'Pending';
        cell6.innerHTML = fetchedJSON[i].category;
        if(cell5.innerHTML === 'Pending')
            cell7.innerHTML = "<button id=respond type='button' onclick='responsefnc(this)'>RESPOND</button>"
        else
            cell7.innerHTML = "<button id=respond type='button' disabled>RESPOND</button>"
    }
    });
}

function responsefnc(e) {
    document.getElementById("givres").value = "";
    var r = e.parentNode.parentNode.rowIndex;
    curr_row = r;
    curr_rec = fetchedJSON[r - 1].id;
    var modal = document.getElementById("myModal");
    modal.style.display = "block";
    var span = document.getElementsByClassName("close")[0];
    span.onclick = function () {
        modal.style.display = "none";
    }
}

/* var span = document.getElementsByClassName("close")[0];
span.onclick = function() {
    modal.style.display = "none";
} */

window.onclick = function(event) {
  if (event.target == modal) {
        modal.style.display = "none";
  }
}
function myFunction() {
        document.getElementById("myDropdown").classList.toggle("show");
}
window.onclick = function(e) {
    if (!e.target.matches('.dropbtn')) {
        var myDropdown = document.getElementById("myDropdown");
        if (myDropdown.classList.contains('show'))
            myDropdown.classList.remove('show');
    }
}
function myFunction1() {
    document.getElementById("myDropdown1").classList.toggle("show");
}
      window.onclick = function(e) {
        if (!e.target.matches('.dropbtn')) {
        var myDropdown = document.getElementById("myDropdown1");
          if (myDropdown.classList.contains('show')) {
        myDropdown.classList.remove('show');
  }
}
}
      function myFunction2() {
        document.getElementById("myDropdown2").classList.toggle("show");
  }
      window.onclick = function(e) {
        if (!e.target.matches('.dropbtn')) {
        var myDropdown = document.getElementById("myDropdown2");
          if (myDropdown.classList.contains('show')) {
        myDropdown.classList.remove('show');
  }
}
}
function myFunction3() {
document.getElementById("myDropdown3").classList.toggle("show");
}
window.onclick = function(e) {
    if (!e.target.matches('.dropbtn')) {
    var myDropdown = document.getElementById("myDropdown3");
    if (myDropdown.classList.contains('show')) {
        myDropdown.classList.remove('show');
    }
    }
}

function submitRes() {
    (document.getElementById("tbl").rows[curr_row].cells[6]).innerHTML = "<button id=respond type='button' disabled>RESPOND</button>";
    (document.getElementById("tbl").rows[curr_row].cells[4]).innerHTML = "Closed";
    console.log(curr_rec);
    console.log(document.getElementById("givres").value);
    fetch('/respondGrievance', {
        method: 'POST',
        headers: {
            "Content-Type": "application/json",
        },
        body: JSON.stringify({ "id": curr_rec, "response": document.getElementById("givres").value}),
    }).then(res => {
        return res.json();
    }).then(function (json) {
        console.log(json)
    });
}

function intotable1() {
    var input, filter, txtValue;
    input = document.getElementById("myInput");
    filter = input.value.toUpperCase();
    var x = document.getElementById("tbl");
    for (var i = 0; i < fetchedJSON.length; i = i + 1) {
        x.rows[i + 1].style.display = "none";
    }
    for (var i = 0; i < fetchedJSON.length; i = i + 1) {
        var str = (fetchedJSON[i].details).concat(fetchedJSON[i].category)
        if (str.toUpperCase().indexOf(filter) > -1) {
            var row = x.insertRow(1);
            var cell1 = row.insertCell(0);
            var cell2 = row.insertCell(1);
            var cell3 = row.insertCell(2);
            var cell4 = row.insertCell(3);
            var cell5 = row.insertCell(4);
            var cell6 = row.insertCell(5);
            var cell7 = row.insertCell(6);
            cell1.innerHTML = fetchedJSON[i].id;
            cell2.innerHTML = fetchedJSON[i].reg_no;
            cell3.innerHTML = fetchedJSON[i].details;
            cell4.innerHTML = fetchedJSON[i].app_date.substring(0,10);
            cell5.innerHTML = fetchedJSON[i].status ? 'Closed' : 'Pending';
            cell6.innerHTML = fetchedJSON[i].category;
            if (cell5.innerHTML === 'Pending')
                cell7.innerHTML = "<button id=respond type='button' onclick='responsefnc(this)'>RESPOND</button>"
            else
                cell7.innerHTML = "<button id=respond type='button' disabled>RESPOND</button>"
        }
        else
            continue;
    }
}
