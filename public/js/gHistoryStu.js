
var fetchedJSON;
function intotable() {
    fetch('/getGrievance', {
        method: 'GET',
        headers: {
            "Content-Type": "application/json",
        },
    }).then(res => {
        return res.json();
    }).then(function (json) {
        fetchedJSON = json;
        console.log(fetchedJSON)
        var x = document.getElementById("tbl");
        for (var i = 0; i < fetchedJSON.length; i = i + 1) {
            var row = x.insertRow(1 + i);
            var cell1 = row.insertCell(0);
            var cell2 = row.insertCell(1);
            var cell3 = row.insertCell(2);
            var cell4 = row.insertCell(3);
            var cell5 = row.insertCell(4);
            var cell6 = row.insertCell(5);
            var cell7 = row.insertCell(6);
            cell1.innerHTML = fetchedJSON[i].id;
            cell2.innerHTML = fetchedJSON[i].details;
            cell3.innerHTML = fetchedJSON[i].app_date.substring(0,10);
            cell4.innerHTML = fetchedJSON[i].level;
            cell5.innerHTML = fetchedJSON[i].category;
            cell6.innerHTML = fetchedJSON[i].status?'Closed':'Pending';
            if (cell6.innerHTML == 'Pending')
                cell7.innerHTML = "<button type='button' disabled>View</button>";
            else if (cell6.innerHTML == 'Closed')
                cell7.innerHTML = "<button type='button' onclick='responsefnc(this)'>View</button>";
        }
    });
}

function responsefnc(e) {
    var modal = document.getElementById("myModal");
    var x = document.getElementById("tbl");
    var row = e.parentNode.parentNode.rowIndex;
    var m = document.getElementById("modresp");
    m.innerHTML = fetchedJSON[row - 1].response;
    console.log(fetchedJSON[row - 1]);
    modal.style.display = "block";
    var span = document.getElementsByClassName("close")[0];
    span.onclick = function () {
        modal.style.display = "none";
    }
}
/*
window.onclick = function(event) {
  if (event.target == modal) {
        modal.style.display = "none";
  }
} */

function intotable1() {
    var input, filter, txtValue;
    input = document.getElementById("myInput");
    filter = input.value.toUpperCase();
    var x = document.getElementById("tbl");
    for (var i = 0; i < fetchedJSON.length; i = i + 1) {
        x.rows[i + 1].style.display = "none";
    }
    for (var i = 0; i < fetchedJSON.length; i = i + 1) {
        var str = (fetchedJSON[i].details).concat(fetchedJSON[i].category)
        if (str.toUpperCase().indexOf(filter) > -1) {
            var row = x.insertRow(1);
            var cell1 = row.insertCell(0);
            var cell2 = row.insertCell(1);
            var cell3 = row.insertCell(2);
            var cell4 = row.insertCell(3);
            var cell5 = row.insertCell(4);
            var cell6 = row.insertCell(5);
            var cell7 = row.insertCell(6);
            cell1.innerHTML = fetchedJSON[i].id;
            cell2.innerHTML = fetchedJSON[i].details;
            cell3.innerHTML = fetchedJSON[i].app_date.substring(0, 10);
            cell4.innerHTML = fetchedJSON[i].level;
            cell5.innerHTML = fetchedJSON[i].category;
            cell6.innerHTML = fetchedJSON[i].status ? 'Closed' : 'Pending';
            if (cell6.innerHTML == 'Pending')
                cell7.innerHTML = "<button type='button' disabled>View</button>";
            else if (cell6.innerHTML == 'Closed')
                cell7.innerHTML = "<button type='button' onclick='responsefnc(this)'>View</button>";
        }
        else
            continue;
    }
}
