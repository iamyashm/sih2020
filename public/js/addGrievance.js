window.addEventListener('load', function () {
    const name = document.getElementById('details')
    const form = document.getElementById('add_grievance_form')
    const error_element = document.getElementById('error')
    const lol = document.getElementById('level');
    const c1 = document.getElementById('category1');
    const c2 = document.getElementById('category2');
    const c3 = document.getElementById('category3');
    var submit = document.getElementById("submit")
    c1.style.display = "none";
    c2.style.display = "none";
    c3.style.display = "none";
    var k;
    lol.addEventListener('click', function (e) {
        e.preventDefault();

        if (lol.value === "U") {
            c1.style.display = "inline";
            c2.style.display = "none";
            c3.style.display = "none";
            k = c1.value;

        }
        if (lol.value === "I") {
            c2.style.display = "inline";
            c1.style.display = "none";
            c3.style.display = "none";
            k = c2.value;

        }
        if (lol.value === "D") {
            c3.style.display = "inline";
            c2.style.display = "none";
            c1.style.display = "none";
            k = c3.value;
        }

    })

    submit.addEventListener('click', (e) => {
        e.preventDefault();
        var a = this.document.getElementById("level");
        var l = a.options[a.selectedIndex].value;
        var cg;
        if(l === 'U')
            cg = "category1";
        else if (l === 'I')
            cg = "category2";
        else
            cg = "category3";
        var b = this.document.getElementById(cg);
        var k = b.options[b.selectedIndex].value;
        data = {
            "details": this.document.getElementById("details").value,
            "category": k,
            "level": l
        }
        console.log(data);
        fetch('/api/addGrievance', {
            method: 'POST',
            headers: {
                "Content-Type": "application/JSON",
            },
            body: JSON.stringify(data),
        }).then(res => {
            return res.json();
        })
            .then(json => {
                if (!json.success) alert(json.msg);
                else {
                    window.location.replace('/profile');
                }
            })
            .catch(function (error) {
                console.log(error);
            });

    })
})
