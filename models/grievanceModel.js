const sql = require('../utils/db.js');
const parsePromise = require('../utils/parsePromise');

var Grievance = function (grievanceObj) {
        this.reg_no = grievanceObj.reg_no;
        this.app_date = grievanceObj.app_date;
        this.level =  grievanceObj.level,
        this.category = grievanceObj.category;
        this.details = grievanceObj.details,
        this.status = grievanceObj.status;
};

Grievance.createGrievance = async function (newGrievance) {
    try {
        var err, result;
        [err, result] = await parsePromise(sql.query("INSERT INTO grievance SET ?", newGrievance));
        if (err) {
            console.log('Error: ' + err);
            return false;
        }
        else {
            console.log('Inserted data');
            return true;
        }
    } catch (err) {
        console.log(err);
        return false;
    }
};

Grievance.getGrievance = async function(acc_type, email) {
    try {
        var level;
        switch (acc_type) {
            case 'u_member':
            case 'u_head':
                level = 'U';
                break;
            case 'i_member':
            case 'i_head':
                level = 'I';
                break;
            case 'd_member':
            case 'd_head':
                level = 'D';
                break;
        }
        var err, result;
        if(acc_type === 'student') {
            [err, result] = await parsePromise(sql.query("SELECT reg_no from student where email='" + email + "'"));
            var reg_no = result[0].reg_no;
            [err, result] = await parsePromise(sql.query("SELECT * from grievance where reg_no=" + reg_no));
        }
        else
            [err, result] = await parsePromise(sql.query("SELECT * from grievance where level='" + level + "'"))
        if(err) {
            console.log(err);
            return null;
        }
        else {
            return result;
        }
    }
    catch(err) {
        console.log(err);
        return null;
    }
};
module.exports = Grievance;
