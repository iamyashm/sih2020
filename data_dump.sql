insert into student values( 10001, 'Yash Maniramka', 'yash@sih.com', 'sih20', 'MIT', 'CSE', 4);
insert into student values( 10002, 'Harshvardhan Rathore', 'hvr@sih.com', 'sih20', 'MIT', 'CSE', 4);
insert into student values( 10003, 'Priyam', 'priyam@sih.com', 'sih20', 'MIT', 'EEE', 4);
insert into student values( 10004,'Paawan', 'paawan@sih.com', 'sih20', 'MIT', 'CSE', 4);
insert into student values( 10005,'Harshita', 'harshita@sih.com', 'sih20', 'MIT', 'CSE', 4);
insert into student values( 10006,'Arpan', 'arpan@sih.com', 'sih20', 'MIT', 'CSE', 4);

insert into grievance(reg_no, app_date, level, category, details, status) values (10001, curdate(), 'U', 'Finance', 'Random grievance 1', 0);
insert into grievance(reg_no, app_date, level, category, details, status) values (10001, curdate(), 'I', 'Hostel', 'Random grievance 2', 0);
insert into grievance(reg_no, app_date, level, category, details, status) values (10001, curdate(), 'D', 'Examination', 'Random grievance 3', 0);
insert into grievance(reg_no, app_date, level, category, details, status) values (10002, curdate(), 'U', 'Finance', 'Random grievance 4', 0);
insert into grievance(reg_no, app_date, level, category, details, status) values (10003, curdate(), 'I', 'Hostel', 'Random grievance 5', 0);
insert into grievance(reg_no, app_date, level, category, details, status) values (10004, curdate(), 'D', 'Examination', 'Random grievance 6', 0);
insert into grievance(reg_no, app_date, level, category, details, status) values (10005, curdate(), 'U', 'Finance', 'Random grievance 7', 0);


insert into u_member(name, email, password, head) values ('Uni Member 1', 'mem1@umem.com', 'sih20', 0);
insert into u_member(name, email, password, head) values ('Uni Member 2', 'mem2@umem.com', 'sih20', 0);
insert into u_member(name, email, password, head) values ('Uni Head', 'head@umem.com', 'sih20', 1);

insert into i_member(name, email, password, head) values ('Inst Member 2', 'mem2@imem.com', 'sih20', 0);
insert into i_member(name, email, password, head) values ('Inst Head', 'head@imem.com', 'sih20', 1);
insert into i_member(name, email, password, head) values ('Inst Member 1', 'mem1@imem.com', 'sih20', 0);

insert into d_member(name, email, password, head) values ('Dept Member 1','mem1@dmem.com', 'sih20', 0);
insert into d_member(name, email, password, head) values ('Dept Member 2','mem2@dmem.com', 'sih20', 0);
insert into d_member(name, email, password, head) values ('Dept Head','head@dmem.com', 'sih20', 1);



