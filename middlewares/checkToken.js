const jwt = require('jsonwebtoken')
const keyconfig = require('../keyconfig')

module.exports = (req, res, next) => {
    let token = req.cookies['jwttoken'];
    if (token) {
        if (token.startsWith('Bearer '))
            token = token.slice(7, token.length)
        jwt.verify(token, keyconfig.secret, (err, decoded) => {
            if (err) {
                return res.json({
                    success: false,
                    message: 'Token is not valid'
                });
            }
            else {
                req.decoded = decoded;
                next();
            }
        })
    }
    else {
        return res.json({
            success: false,
            message: 'Not logged in'
        });
    }
};

