const router = require('express').Router();
const {loginPost} = require('./login');
const {profileGet} = require('./profile');
const logoutGet = require('./logout');
const checkToken = require('../middlewares/checkToken');
const addGrievancePost = require('./addGrievance');
const getGrievance = require('./getGrievance');
const addMember = require('./addMember');
const delMember = require('./delMember');
const respondGrievance = require('./respondGrievance');
const getHome  = require('./getHome');
const showProfile = require('./showProfile');
const getStatus = require('./getStatus');
const addGrievanceGet  = require('./addGrievanceGet');
const manageMem = require('./manageMem.js');

router.get('/', getHome);
router.get('/status', checkToken, getStatus);
router.post('/login', loginPost);
router.get('/profile', showProfile)
router.get('/api/getProfile', checkToken, profileGet);
router.get('/logout', checkToken, logoutGet);
router.get('/addGrievance', checkToken, addGrievanceGet);
router.post('/api/addGrievance', checkToken, addGrievancePost);
router.get('/getGrievance', checkToken, getGrievance);
router.get('/manageMember', checkToken, manageMem);
router.post('/addMember', checkToken, addMember);
router.post('/delMember', checkToken, delMember);
router.post('/respondGrievance', checkToken, respondGrievance);

module.exports = router;

//ION@mithk9
