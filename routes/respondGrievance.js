const sql = require('../utils/db');
const parsePromise = require('../utils/parsePromise');

module.exports = async (req, res) => {
    try {
        var acc_type = req.decoded.acc_type;
        if (acc_type === 'student') {
            return res.json({ success: false, message: 'Unauthorized' });
        }
        else {
            console.log(req.body);
            var id = req.body.id;
            var response = req.body.response;
            var query = "update grievance set status=1, response=" + sql.escape(response) + " where id=" + id;
            var err, result;
            [err, result] = await parsePromise(sql.query(query));
            if (err) {
                console.log(err);
                return res.status(400).json({ success: false });
            }
            else {
                console.log('Updated grievance status');
                return res.json({ success: true, message: 'Updated Grievance Status' });
            }
        }
    }
    catch (err) {
        console.log(err);
        return res.status(400).json({ success: false, message: 'Error has occured' });
    }
};
