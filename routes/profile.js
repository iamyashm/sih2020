const sql = require('../utils/db');
const parsePromise = require('../utils/parsePromise');

var profileGet = async (req, res) => {
    try {
        var email = req.decoded.email;
        var acc_type = req.decoded.acc_type;
        console.log('Hey ' + email);
        if(acc_type === 'student') {
            var searchquery = "select * from student where email='" + email + "'";
            var err, result;
            [err, result] = await parsePromise(sql.query(searchquery));
            if(result.length) {
                var name = result[0].name;
                var college = result[0].college;
                var reg_no = result[0].reg_no;
                var sem = result[0].sem;
                var stream = result[0].stream;
                return res.json({ success:  true, type: 'student', name: name, college: college, reg_no: reg_no, stream: stream, sem: sem });
            }
            else {
                console.log(err);
                return res.status(400).json({ message: 'Error '});
            }
        }
        else if(acc_type === 'u_member' || acc_type === 'i_member' || acc_type === 'd_member') {
            var y;
            if (acc_type === 'u_member') {
                y = 'University';
            }
            else if (acc_type === 'd_member'){
                y = 'Department';
            }
            else {
                y = 'Institute';
            }
            var searchquery = "select * from " + acc_type + " where email='" + email + "'";
            var err, result;
            [err, result] = await parsePromise(sql.query(searchquery));
            if (result.length) {
                var name = result[0].name;
                return res.json({ success: true, name: name, email: email, type: 'Committee Member' , level: y });
            }
            else {
                console.log(err);
                return res.status(400).json({ message: 'Error ' });
            }
        }
        else {
            var x, y;
            if(acc_type === 'u_head') {
                x = 'u_member';
                y = 'University';
            }
            else if (acc_type === 'd_head') {
                x = 'd_member';
                y = 'Department';
            }
            else {
                x = 'i_member';
                y = 'Institute';
            }
            var searchquery = "select * from " + x + " where email='" + email + "' and head=1";
            var err, result;
            [err, result] = await parsePromise(sql.query(searchquery));
            if (result.length) {
                var name = result[0].name;
                return res.json({ success: true, name: name, email: email, type: 'Committee Head', level: y });
            }
            else {
                console.log(err);
                return res.status(400).json({ message: 'Error ' });
            }
        }
    }
    catch(err) {
        return res.status(400).json({message: 'Error has occured'});
    }
};

module.exports = { profileGet: profileGet };
