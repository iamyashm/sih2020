const sql = require('../utils/db');
const parsePromise = require('../utils/parsePromise');
const Grievance = require('../models/grievanceModel');

module.exports = async (req, res) => {
    try {
        var email = req.decoded.email;
        var level = req.body.level;
        var details = req.body.details;
        var category = req.body.category;
        console.log(req.body);
        var studentquery = "select reg_no from student where email='" + email + "'";
        var err, result;
        [err, result] = await parsePromise(sql.query(studentquery));
        if(result.length) {
            var reg_no = result[0].reg_no;
            var new_grievance = new Grievance({reg_no: reg_no, app_date: new Date(), level: level, category: category, details: details, status: 0 });
            var flag = await Grievance.createGrievance(new_grievance)
            if(flag)
                return res.json({ success: true, message: 'Grievance added'});
            else
                return res.json({success: false, message: 'Error. Grievance not added.' });
        }
        else {
            console.log('Error');
            return res.status(400).json({ success: false });
        }
    }
    catch (err){
        console.log(err);
        return res.status(400).json({ success: false, message: 'Error has occured' });
    }
};
