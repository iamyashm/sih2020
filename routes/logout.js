module.exports = (req, res) => {
    res.clearCookie('jwttoken');
    return res.redirect('../');
    //return res.json({success: true, message: 'Logged out successfully'});
};
