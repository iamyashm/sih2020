const sql = require('../utils/db');
const parsePromise = require('../utils/parsePromise');
const Grievance = require('../models/grievanceModel');

module.exports = async (req, res) => {
    try {
        var acc_type = req.decoded.acc_type;
        var email = req.decoded.email;
        var result = await Grievance.getGrievance(acc_type, email);
        if(result) {
            return res.json(result);
        }
        else {
            return res.json({ success: false, message: 'Error' });
        }
    } catch (error) {
        console.log(error);
        return res.json({success: false, message: 'Error'});
    }
}
