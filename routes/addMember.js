const sql = require('../utils/db');
const parsePromise = require('../utils/parsePromise');

module.exports = async (req, res) => {
    try {
        var acc_type = req.decoded.acc_type;
        if(acc_type === 'u_head' || acc_type === 'd_head' || acc_type === 'i_head') {
            var name = req.body.name;
            var email = req.body.email;
            var password = req.body.password;
            var tablename = acc_type.substring(0, 2) + 'member';
            var query = "insert into " + tablename + " set ?";
            var err, result;
            [err, result] = await parsePromise(sql.query(query, {name: name, email: email, password: password}));
            if(err) {
                console.log(err);
                return res.status(400).json({ success: false });
            }
            else {
                console.log('Added member');
                return res.redirect('../profile');
                //return res.json({ success: true, message: 'Member added'});
            }
        }
        else {
            return res.json({success: false, message: 'Unauthorized'});
        }
    }
    catch(err) {
        console.log(err);
        return res.status(400).json({ success: false, message: 'Error has occured'});
    }
};
