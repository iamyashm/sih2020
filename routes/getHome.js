
module.exports = (req, res) => {
    if(req.cookies['jwttoken'] === undefined) {
        res.render('login');
    }
    else {
        res.redirect('/profile');
    }
};
