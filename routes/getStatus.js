module.exports = (req, res) => {
    if(req.decoded.acc_type === 'student')
        return res.render('gHistoryStu');
    else
        return res.render('gHistoryMem');
};
