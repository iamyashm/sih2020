const sql = require('../utils/db');
const jwt = require('jsonwebtoken');
const keyconfig = require('../keyconfig');
const parsePromise = require('../utils/parsePromise');

var loginPost = async (req, res) => {
    try {
        var email = req.body.email;
        var password = req.body.password;
        var acc_type = req.body.acc_type;
        var searchquery;
        if(acc_type === 'student')
            searchquery = "select * from " + acc_type + " where reg_no=" + Number(email) + " and password='" + password + "'";
        else if ( acc_type === 'u_member' || acc_type === 'i_member' || acc_type === 'd_member')
            searchquery = "select * from " + acc_type + " where email='" + email + "' and password='" + password + "'";
        else if(acc_type === 'u_head')
            searchquery = "select * from u_member where email='" + email + "' and password='" + password + "' and head=1";
        else if (acc_type === 'd_head')
            searchquery = "select * from d_member where email='" + email + "' and password='" + password + "' and head=1";
        else
            searchquery = "select * from i_member where email='" + email + "' and password='" + password + "' and head=1";
        console.log(searchquery);
        var err, result;
        [err, result] = await parsePromise(sql.query(searchquery));
        if(result.length) {
            let token = jwt.sign(
                { email: result[0].email, acc_type: acc_type },
                keyconfig.secret,
                { expiresIn: '24h' }
            );
            res.cookie('jwttoken', token, { maxAge: 3600 * 24 * 1000, httpOnly: true })
            res.redirect('../profile')
            //return res.json({ success: true, message: 'Login successful' });
        }
        else {
            console.log(err);
            return res.json({ success: false, message: 'Username/password incorrect' });
        }
    }
    catch(err) {
        console.log(err);
        return res.status(400).json({ success: false, message: 'error' });
    }
};

module.exports = { loginPost: loginPost };
